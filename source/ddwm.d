//#####################################################
// File: ddwm.d
// Created: 2014-12-22 22:08:34
// Modified: 2015-02-21 20:58:21
//
// See LICENSE file for license and copyright details.
//#####################################################

/**
 * dynamic window manager is designed like any other X client as well. It is
 * driven through handling X events. In contrast to other X clients, a window
 * manager selects for SubstructureRedirectMask on the rootWin window, to receive
 * events about window (dis-)appearance.  Only one X connection at a time is
 * allowed to select for this event mask.
 *
 * The event handlers of dwm are organized in an array which is accessed
 * whenever a new event has been fetched. This allows event dispatching
 * in O(1) time.
 *
 * Each child of the rootWin window is called a client, except windows which have
 * set the override_redirect flag.  Clients are organized in a linked client
 * list on each monitor, the focus history is remembered through a stack list
 * on each monitor. Each client contains a bit array to indicate the tags of a
 * client.
 *
 * Keys and tagging rules are organized as arrays and defined in config.h.
 *
 * To understand everything else, start reading main().
 */
module ddwm;


import core.memory;
alias DGC = core.memory.GC;

import core.sys.posix.signal;
import core.sys.posix.sys.wait;
import core.sys.posix.unistd;

import std.c.locale;
import std.c.string;
import std.c.stdlib;


import std.stdio;
import std.string;
import std.algorithm;
import std.conv;
import std.process;
import std.traits;

import deimos.X11.X;
import deimos.X11.Xlib;
import deimos.X11.keysymdef;
import deimos.X11.Xutil;
import deimos.X11.Xatom;


import ddwmtypes;
import ddwmutil;
import _drw;
import Xinerama;
import cursorfont;

// From core.sys.posix.sys.wait because waitpid is not nothrow @nogc @system
extern(C) pid_t waitpid(pid_t, int*, int) nothrow @nogc @system;


// From core.stdc.stdio due to conflict with std.stdio over stderr
extern(C) int close(int fd) @trusted;

//###################################################
// CONFIG
//###################################################
static immutable string VERSION = "0.1.1.dlang";

/* appearance */
static immutable string font            = "-*-terminus-medium-r-*-*-16-*-*-*-*-*-*-*";
static immutable string normbordercolor = "#444444";
static immutable string normbgcolor     = "#222222";
static immutable string normfgcolor     = "#bbbbbb";
static immutable string selbordercolor  = "#550077";
static immutable string selbgcolor      = "#550077";
static immutable string selfgcolor      = "#eeeeee";
static immutable uint borderpx  = 1;        /* border pixel of windows */
static immutable uint snap      = 32;       /* snap pixel */
static immutable bool showbar           = true;     /* false means no bar */
static immutable bool topbar            = true;     /* false means bottom bar */

/* tagging */
immutable string[] tags = [ "1", "2", "3", "4", "5", "6", "7", "8", "9" ];

static immutable Rule[] rules = [
                                    /* xprop(1):
                                     *	WM_CLASS(STRING) = instance, klass
                                     *	WM_NAME(STRING) = title
                                     */
                                    /* klass      instance    title       tags mask     isfloating   monitor */
{ "Gimp",     null,       null,       0,            true,        -1 },
{ "Firefox",  null,       null,       1 << 8,       false,       -1 },
                                ];

/* layout(s) */
static immutable float mfact      = 0.55; /* factor of master area size [0.05..0.95] */
static immutable int nmaster      = 1;    /* number of clients in master area */
static immutable bool resizehints = true; /* true means respect size hints in tiled resizals */

static immutable Layout[] layouts = [
                                        /* symbol     arrange function */
{ symbol:"[]=",      arrange:&tile },    /* first entry is default */
{ symbol:"><>",      arrange:null },    /* no layout function means floating behavior */
{ symbol:"[M]",      arrange:&monocle },
                                    ];

/* key definitions */
enum MODKEY = Mod1Mask;


/* commands */
static char[2] dmenumon = "0"; /* component of dmenucmd, manipulated in spawn() */
static immutable string[] dmenucmd = [ "dmenu_run", "-fn", font, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor];
static immutable string[] termcmd = ["uxterm"];

static Key[] keys;

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button[] buttons;

static this() {

    keys = [
        Key( MODKEY,                       XK_p,      &spawn,         dmenucmd ), // dmenucmd
        Key( MODKEY|ShiftMask,             XK_Return, &spawn,          termcmd ), // termcmd
        Key( MODKEY,                       XK_b,      &togglebar       ),
        Key( MODKEY,                       XK_j,      &focusstack,     +1  ),
        Key( MODKEY,                       XK_k,      &focusstack,     -1  ),
        Key( MODKEY,                       XK_i,      &incnmaster,     +1  ),
        Key( MODKEY,                       XK_d,      &incnmaster,     -1  ),
        Key( MODKEY,                       XK_h,      &setmfact,       -0.05 ),
        Key( MODKEY,                       XK_l,      &setmfact,       +0.05 ),
        Key( MODKEY,                       XK_Return, &zoom            ),
        Key( MODKEY,                       XK_Tab,    &view            ),
        Key( MODKEY|ShiftMask,             XK_c,      &killclient      ),
        Key( MODKEY,                       XK_t,      &setlayout,      &layouts[0] ),
        Key( MODKEY,                       XK_f,      &setlayout,      &layouts[1] ),
        Key( MODKEY,                       XK_m,      &setlayout,      &layouts[2] ),
        Key( MODKEY,                       XK_space,  &setlayout,      0 ),
        Key( MODKEY|ShiftMask,             XK_space,  &togglefloating, 0 ),
        Key( MODKEY,                       XK_0,      &view,           ~0  ),
        Key( MODKEY|ShiftMask,             XK_0,      &tag,            ~0  ),
        Key( MODKEY,                       XK_comma,  &focusmon,       -1  ),
        Key( MODKEY,                       XK_period, &focusmon,       +1  ),
        Key( MODKEY|ShiftMask,             XK_comma,  &tagmon,         -1  ),
        Key( MODKEY|ShiftMask,             XK_period, &tagmon,         +1  ),
        Key( MODKEY,                       XK_1,      &view,           1<<0 ),
        Key( MODKEY|ControlMask,           XK_1,      &toggleview,     1<<0 ),
        Key( MODKEY|ShiftMask,             XK_1,      &tag,            1<<0 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_1,      &toggletag,      1<<0 ),
        Key( MODKEY,                       XK_2,      &view,           1<<1 ),
        Key( MODKEY|ControlMask,           XK_2,      &toggleview,     1<<1 ),
        Key( MODKEY|ShiftMask,             XK_2,      &tag,            1<<1 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_2,      &toggletag,      1<<1 ),
        Key( MODKEY,                       XK_3,      &view,           1<<2 ),
        Key( MODKEY|ControlMask,           XK_3,      &toggleview,     1<<2 ),
        Key( MODKEY|ShiftMask,             XK_3,      &tag,            1<<2 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_3,      &toggletag,      1<<2 ),
        Key( MODKEY,                       XK_4,      &view,           1<<3 ),
        Key( MODKEY|ControlMask,           XK_4,      &toggleview,     1<<3 ),
        Key( MODKEY|ShiftMask,             XK_4,      &tag,            1<<3 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_4,      &toggletag,      1<<3 ),
        Key( MODKEY,                       XK_5,      &view,           1<<4 ),
        Key( MODKEY|ControlMask,           XK_5,      &toggleview,     1<<4 ),
        Key( MODKEY|ShiftMask,             XK_5,      &tag,            1<<4 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_5,      &toggletag,      1<<4 ),
        Key( MODKEY,                       XK_6,      &view,           1<<5 ),
        Key( MODKEY|ControlMask,           XK_6,      &toggleview,     1<<5 ),
        Key( MODKEY|ShiftMask,             XK_6,      &tag,            1<<5 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_6,      &toggletag,      1<<5 ),
        Key( MODKEY,                       XK_7,      &view,           1<<6 ),
        Key( MODKEY|ControlMask,           XK_7,      &toggleview,     1<<6 ),
        Key( MODKEY|ShiftMask,             XK_7,      &tag,            1<<6 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_7,      &toggletag,      1<<6 ),
        Key( MODKEY,                       XK_8,      &view,           1<<7 ),
        Key( MODKEY|ControlMask,           XK_8,      &toggleview,     1<<7 ),
        Key( MODKEY|ShiftMask,             XK_8,      &tag,            1<<7 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_8,      &toggletag,      1<<7 ),
        Key( MODKEY,                       XK_9,      &view,           1<<8 ),
        Key( MODKEY|ControlMask,           XK_9,      &toggleview,     1<<8 ),
        Key( MODKEY|ShiftMask,             XK_9,      &tag,            1<<8 ),
        Key( MODKEY|ControlMask|ShiftMask, XK_9,      &toggletag,      1<<8 ),
        Key( MODKEY|ShiftMask,             XK_q,      &quit                 )
            ];

    buttons = [
        /* click                event mask      button          function        argument */
        Button( ClkLtSymbol,          0,              Button1,        &setlayout ),
        Button( ClkLtSymbol,          0,              Button3,        &setlayout,      &layouts[2] ),
        Button( ClkWinTitle,          0,              Button2,        &zoom ),
        Button( ClkStatusText,        0,              Button2,        &spawn,          termcmd ), // &termcmd
        Button( ClkClientWin,         MODKEY,         Button1,        &movemouse ),
        Button( ClkClientWin,         MODKEY,         Button2,        &togglefloating ),
        Button( ClkClientWin,         MODKEY,         Button3,        &resizemouse ),
        Button( ClkTagBar,            0,              Button1,        &view ),
        Button( ClkTagBar,            0,              Button3,        &toggleview ),
        Button( ClkTagBar,            MODKEY,         Button1,        &tag ),
        Button( ClkTagBar,            MODKEY,         Button3,        &toggletag )
    ];



    handler[ButtonPress] = &buttonpress;
    handler[ClientMessage] = &clientmessage;
    handler[ConfigureRequest] = &configurerequest;
    handler[ConfigureNotify] = &configurenotify;
    handler[DestroyNotify] = &destroynotify;
    handler[EnterNotify] = &enternotify;
    handler[Expose] = &expose;
    handler[FocusIn] = &focusin;
    handler[KeyPress] = &keypress;
    handler[MappingNotify] = &mappingnotify;
    handler[MapRequest] = &maprequest;
    handler[MotionNotify] = &motionnotify;
    handler[PropertyNotify] = &propertynotify;
    handler[UnmapNotify] = &unmapnotify;

    /*extern(C) void sigsegvHandler(int i) nothrow @nogc @system {
      printf("\n\n\n>>>> sigsegv <<<<\n\n\n");
      exit(EXIT_FAILURE);
      }
      signal(SIGSEGV, &sigsegvHandler);*/
}


//###################################################
// END CONFIG
//###################################################
//###################################################
// VARIABLES
immutable string broken = "broken";
static string stext;
static int screen;
static int sw, sh;           /* X display screen geometry width, height */
static int bh, blw = 0;      /* bar geometry */
extern(C) static int function(Display*, XErrorEvent*) nothrow xerrorxlib;
static uint numlockmask = 0;
static void function(XEvent*)[LASTEvent] handler;
/*extern(C) {
    // These are passed to X so __gshared?
    __gshared Atom wmatom[WMLast];
    __gshared Atom netatom[NetLast];
    __gshared Display *dpy;
    __gshared Monitor *mons, selmon;
    __gshared Window rootWin;
}*/
static Atom[WMLast] wmatom;
static Atom[NetLast] netatom;
static Display *dpy;
static Monitor *mons, selmon;
static Window rootWin;

static bool running = true;
static Cur*[CurLast] cursor;
static ClrScheme[SchemeLast] scheme;
static Drw *drw;
static Fnt *fnt;


//###################################################
// MACROS
enum BUTTONMASK = ButtonPressMask | ButtonReleaseMask;
auto CLEANMASK(M)(auto ref in M mask) {
    return (mask & ~(numlockmask|LockMask) & (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask));
}
auto INTERSECT(T, M)(T x, T y, T w, T h, M m) {
    import std.algorithm;
    return max(0, min(x + w, m.wx + m.ww) - max(x, m.wx)) * max(0, min(y + h, m.wy + m.wh) - max(y, m.wy));
}
auto ISVISIBLE(C)(auto ref in C c) pure @safe @nogc nothrow {
    return c.tags & c.mon.tagset[c.mon.seltags];
}
auto LENGTH(X)(auto ref in X x) {
    return x.length;
}
enum MOUSEMASK = ButtonPressMask | ButtonReleaseMask | PointerMotionMask;

auto WIDTH(X)(auto ref in X x) {
    return x.w + 2 * x.bw;
}
auto HEIGHT(X)(auto ref in X x) {
    return x.h + 2 * x.bw;
}
enum TAGMASK = ((1 << tags.length) - 1);
auto TEXTW(X)(auto ref in X x)
if(isSomeString!X) {
    return drw.font.getexts_width(x) + drw.font.h;
}
//###################################################

/* compile-time check if all tags fit into an uint bit array. */
static assert(tags.length < 31, "tags excceeds 32 bit limit.");
//###################################################
// FUNCTIONS
//###################################################
void applyrules(Client *c) {
    

    XClassHint ch = { null, null };
    /* rule matching */
    c.isfloating = false;
    c.tags = 0;
    XGetClassHint(dpy, c.win, &ch);
    immutable auto klass    = ch.res_class ? ch.res_class.to!string : broken;
    immutable auto instance = ch.res_name  ? ch.res_name.to!string : broken;
    foreach(immutable r; rules) {
        if( (r.title.length==0 || r.title.indexOf(c.name) >= 0) &&
                (r.klass.length==0 || r.klass.indexOf(klass) >= 0) &&
                (r.instance.length==0 || r.instance.indexOf(instance) >= 0)) {
            c.isfloating = r.isfloating;
            c.tags |= r.tags;

            auto m = mons.range.find!(mon => mon.num == r.monitor).front;
            if(m) {
                c.mon = m;
            }
        }
    }
    if(ch.res_class)
        XFree(ch.res_class);
    if(ch.res_name)
        XFree(ch.res_name);
    c.tags = c.tags & TAGMASK ? c.tags & TAGMASK : c.mon.tagset[c.mon.seltags];
}

bool applysizehints(Client *c, ref int x, ref int y, ref int w, ref int h, bool interact) {
    
    bool baseismin;
    Monitor *m = c.mon;

    /* set minimum possible */
    w = max(1, w);
    h = max(1, h);
    if(interact) {
        if(x > sw)
            x = sw - WIDTH(c);
        if(y > sh)
            y = sh - HEIGHT(c);
        if(x + w + 2 * c.bw < 0)
            x = 0;
        if(y + h + 2 * c.bw < 0)
            y = 0;
    } else {
        if(x >= m.wx + m.ww)
            x = m.wx + m.ww - WIDTH(c);
        if(y >= m.wy + m.wh)
            y = m.wy + m.wh - HEIGHT(c);
        if(x + w + 2 * c.bw <= m.wx)
            x = m.wx;
        if(y + h + 2 * c.bw <= m.wy)
            y = m.wy;
    }
    if(h < bh)
        h = bh;
    if(w < bh)
        w = bh;
    if(resizehints || c.isfloating || !c.mon.lt[c.mon.sellt].arrange) {
        /* see last two sentences in ICCCM 4.1.2.3 */
        baseismin = c.basew == c.minw && c.baseh == c.minh;
        if(!baseismin) { /* temporarily remove base dimensions */
            w -= c.basew;
            h -= c.baseh;
        }
import std.math :
        nearbyint;
        /* adjust for aspect limits */
        if(c.mina > 0 && c.maxa > 0) {
            if(c.maxa < float(w) / h)
                w = cast(int)(h * c.maxa + 0.5);
            else if(c.mina < float(h) / w)
                h = cast(int)(w * c.mina + 0.5);
        }
        if(baseismin) { /* increment calculation requires this */
            w -= c.basew;
            h -= c.baseh;
        }
        /* adjust for increment value */
        if(c.incw)
            w -= w % c.incw;
        if(c.inch)
            h -= h % c.inch;
        /* restore base dimensions */
        w = max(w + c.basew, c.minw);
        h = max(h + c.baseh, c.minh);
        if(c.maxw)
            w = min(w, c.maxw);
        if(c.maxh)
            h = min(h, c.maxh);
    }
    return x != c.x || y != c.y || w != c.w || h != c.h;
}


void arrange(Monitor *m) {
    
    if(m) {
        showhide(m.stack);
    } else foreach(m; mons.range) {
        showhide(m.stack);
    }
    if(m) {
        arrangemon(m);
        restack(m);
    } else foreach(m; mons.range) {
        arrangemon(m);
    }
}

void arrangemon(Monitor *m) {
    
    m.ltsymbol = m.lt[m.sellt].symbol;

    if(m.lt[m.sellt].arrange)
        m.lt[m.sellt].arrange(m);
}

void attach(Client *c) {
    
    c.next = c.mon.clients;
    c.mon.clients = c;
}

void attachstack(Client *c) {
    
    c.snext = c.mon.stack;
    c.mon.stack = c;
}

void buttonpress(XEvent *e) {
    
    uint i, x, click;
    auto arg = Arg(0);
    Client *c;
    Monitor *m;
    XButtonPressedEvent *ev = &e.xbutton;

    click = ClkRootWin;
    /* focus monitor if necessary */
    m = wintomon(ev.window);
    if( (m !is null) && (m != selmon) ) {
        unfocus(selmon.sel, true);
        selmon = m;
        focus(null);
    }
    if(ev.window == selmon.barwin) {
        i = x = 0;
        do {
            x += TEXTW(tags[i]);
        } while(ev.x >= x && ++i < LENGTH(tags));
        if(i < LENGTH(tags)) {
            click = ClkTagBar;
            arg.ui = 1 << i;
        } else if(ev.x < x + blw)
            click = ClkLtSymbol;
        else if(ev.x > selmon.ww - TEXTW(stext))
            click = ClkStatusText;
        else
            click = ClkWinTitle;
    } else {
        c = wintoclient(ev.window);
        if(c !is null) {
            focus(c);
            click = ClkClientWin;
        }
    }
    foreach(ref const but; buttons) {
        if(click == but.click &&
                but.func !is null &&
                but.button == ev.button &&
                CLEANMASK(but.mask) == CLEANMASK(ev.state)) {
            but.func(click == ClkTagBar && but.arg.i == 0 ? &arg : &but.arg);
        }
    }
}

void checkotherwm() {
    
    xerrorxlib = XSetErrorHandler(&xerrorstart);
    /* this causes an error if some other window manager is running */
    XSelectInput(dpy, DefaultRootWindow(dpy), SubstructureRedirectMask);
    XSync(dpy, false);
    XSetErrorHandler(&xerror);
    XSync(dpy, false);
}


void cleanup() {
    
    auto a = Arg(-1);
    Layout foo = { "", null };

    view(&a);
    selmon.lt[selmon.sellt] = &foo;
    foreach(m; mons.range) {
        while(m.stack) {
            unmanage(m.stack, false);
        }
    }
    XUngrabKey(dpy, AnyKey, AnyModifier, rootWin);
    while(mons) {
        cleanupmon(mons);
    }
    Cur.free(cursor[CurNormal]);
    Cur.free(cursor[CurResize]);
    Cur.free(cursor[CurMove]);
    Fnt.free(dpy, fnt);
    Clr.free(scheme[SchemeNorm].border);
    Clr.free(scheme[SchemeNorm].bg);
    Clr.free(scheme[SchemeNorm].fg);
    Clr.free(scheme[SchemeSel].border);
    Clr.free(scheme[SchemeSel].bg);
    Clr.free(scheme[SchemeSel].fg);
    Drw.free(drw);
    XSync(dpy, false);
    XSetInputFocus(dpy, PointerRoot, RevertToPointerRoot, CurrentTime);
    XDeleteProperty(dpy, rootWin, netatom[NetActiveWindow]);
}

void cleanupmon(Monitor *mon) {
    if(mon && mon == mons) {
        mons = mons.next;
    } else {
        auto m = mons.range.find!(a => a.next == mon).front;
        if(m) {
            m.next = mon.next;
        }
    }
    XUnmapWindow(dpy, mon.barwin);
    XDestroyWindow(dpy, mon.barwin);
    DGC.free(mon);
}

void clearurgent(Client *c) {
    
    XWMHints *wmh;

    c.isurgent = false;
    wmh = XGetWMHints(dpy, c.win);
    if(wmh is null) {
        return;
    }
    wmh.flags &= ~XUrgencyHint;
    XSetWMHints(dpy, c.win, wmh);
    XFree(wmh);
}

void clientmessage(XEvent *e) {
    
    XClientMessageEvent *cme = &e.xclient;
    Client *c = wintoclient(cme.window);

    if(!c)
        return;
    if(cme.message_type == netatom[NetWMState]) {
        if(cme.data.l[1] == netatom[NetWMFullscreen] || cme.data.l[2] == netatom[NetWMFullscreen]) {
            setfullscreen(c, (cme.data.l[0] == 1 /* _NET_WM_STATE_ADD    */
                              || (cme.data.l[0] == 2 /* _NET_WM_STATE_TOGGLE */ && !c.isfullscreen)));
        }
    } else if(cme.message_type == netatom[NetActiveWindow]) {
        if(!ISVISIBLE(c)) {
            c.mon.seltags ^= 1;
            c.mon.tagset[c.mon.seltags] = c.tags;
        }
        pop(c);
    }
}

void configure(Client *c) {
    
    XConfigureEvent ce;

    ce.type = ConfigureNotify;
    ce.display = dpy;
    ce.event = c.win;
    ce.window = c.win;
    ce.x = c.x;
    ce.y = c.y;
    ce.width = c.w;
    ce.height = c.h;
    ce.border_width = c.bw;
    ce.above = None;
    ce.override_redirect = false;
    XSendEvent(dpy, c.win, false, StructureNotifyMask, cast(XEvent *)&ce);
}

void configurenotify(XEvent *e) {
    
    XConfigureEvent *ev = &e.xconfigure;
    bool dirty;

    // TODO: updategeom handling sucks, needs to be simplified
    if(ev.window == rootWin) {
        dirty = (sw != ev.width || sh != ev.height);
        sw = ev.width;
        sh = ev.height;
        if(updategeom() || dirty) {
            drw.resize(sw, bh);
            updatebars();
            foreach(m; mons.range) {
                XMoveResizeWindow(dpy, m.barwin, m.wx, m.by, m.ww, bh);
            }
            focus(null);
            arrange(null);
        }
    }
}

void configurerequest(XEvent *e) {
    
    Client *c;
    Monitor *m;
    XConfigureRequestEvent *ev = &e.xconfigurerequest;
    XWindowChanges wc;
    c = wintoclient(ev.window);
    if(c) {
        if(ev.value_mask & CWBorderWidth) {
            c.bw = ev.border_width;
        } else if(c.isfloating || !selmon.lt[selmon.sellt].arrange) {
            m = c.mon;
            if(ev.value_mask & CWX) {
                c.oldx = c.x;
                c.x = m.mx + ev.x;
            }
            if(ev.value_mask & CWY) {
                c.oldy = c.y;
                c.y = m.my + ev.y;
            }
            if(ev.value_mask & CWWidth) {
                c.oldw = c.w;
                c.w = ev.width;
            }
            if(ev.value_mask & CWHeight) {
                c.oldh = c.h;
                c.h = ev.height;
            }
            if((c.x + c.w) > m.mx + m.mw && c.isfloating)
                c.x = m.mx + (m.mw / 2 - WIDTH(c) / 2); /* center in x direction */
            if((c.y + c.h) > m.my + m.mh && c.isfloating)
                c.y = m.my + (m.mh / 2 - HEIGHT(c) / 2); /* center in y direction */
            if((ev.value_mask & (CWX|CWY)) && !(ev.value_mask & (CWWidth|CWHeight)))
                configure(c);
            if(ISVISIBLE(c))
                XMoveResizeWindow(dpy, c.win, c.x, c.y, c.w, c.h);
        } else {
            configure(c);
        }
    } else {
        wc.x = ev.x;
        wc.y = ev.y;
        wc.width = ev.width;
        wc.height = ev.height;
        wc.border_width = ev.border_width;
        wc.sibling = ev.above;
        wc.stack_mode = ev.detail;

        // HACK to fix the slowdown XError issue. value_mask recieved is 36 but needs to be 12
        // 36 ==> b2:width, b5:sibling
        // 12 ==> b2:width, b3:height
        ev.value_mask = 12;
        XConfigureWindow(dpy, ev.window, ev.value_mask, &wc);
    }
    XSync(dpy, false);
}

Monitor* createmon() {
    
    Monitor* m = new Monitor();

    import std.string;
    if(m is null) {
        die("fatal: could not malloc() %s bytes\n", Monitor.sizeof);
    }
    m.tagset[0] = m.tagset[1] = 1;
    m.mfact = mfact;
    m.nmaster = nmaster;
    m.showbar = showbar;
    m.topbar = topbar;
    m.lt[0] = &layouts[0];
    m.lt[1] = &layouts[1 % LENGTH(layouts)];
    m.ltsymbol = layouts[0].symbol;
    //strncpy(m.ltsymbol.ptr, layouts[0].symbol, m.ltsymbol.sizeof);
    return m;
}
void destroynotify(XEvent *e) {
    
    Client *c;
    XDestroyWindowEvent *ev = &e.xdestroywindow;

    c = wintoclient(ev.window);
    if(c !is null) {
        unmanage(c, true);
    }
}

void detach(Client *c) {
    
    Client **tc;
    for(tc = &c.mon.clients; *tc && *tc != c; tc = &(*tc).next) {}
    *tc = c.next;
}

void detachstack(Client *c) {
    
    Client **tc;


    for(tc = &c.mon.stack; *tc && *tc != c; tc = &(*tc).snext) {}
    *tc = c.snext;

    if(c && c == c.mon.sel) {
        auto t = c.mon.stack.range!"snext".find!(a=>ISVISIBLE(a)).front;
        c.mon.sel = t;
    }
}

Monitor* dirtomon(int dir) {
    
    Monitor *m = null;

    if(dir > 0) {
        m = selmon.next;
        if(m is null) {
            m = mons;
        }
    } else if(selmon == mons) {
        m = mons.range.find!"a.next is null".front;
    } else {
        m = mons.range.find!(a => a.next == selmon).front;
    }
    return m;
}

void drawbar(Monitor *m) {
    
    uint occ = 0, urg = 0;

    foreach(c; m.clients.range!"next") {
        occ |= c.tags;
        if(c.isurgent) {
            urg |= c.tags;
        }
    }
    int x = 0, w;
    foreach(i, tag; tags) {
        w = TEXTW(tag);
        drw.setscheme((m.tagset[m.seltags] & (1 << i)) ? &scheme[SchemeSel] : &scheme[SchemeNorm]);
        drw.text(x, 0, w, bh, tag, urg & 1 << i);
        drw.rect(x, 0, w, bh, m == selmon && selmon.sel && selmon.sel.tags & 1 << i,
                 occ & 1 << i, urg & 1 << i);
        x += w;
    }
    w = blw = TEXTW(m.ltsymbol);
    drw.setscheme(&scheme[SchemeNorm]);
    drw.text(x, 0, w, bh, m.ltsymbol, 0);
    x += w;
    int xx = x;
    if(m == selmon) { /* status is only drawn on selected monitor */
        w = TEXTW(stext);
        x = m.ww - w;
        if(x < xx) {
            x = xx;
            w = m.ww - xx;
        }
        drw.text(x, 0, w, bh, stext, 0);
    } else {
        x = m.ww;
    }
    if((w = x - xx) > bh) {
        x = xx;
        if(m.sel) {
            drw.setscheme(m == selmon ? &scheme[SchemeSel] : &scheme[SchemeNorm]);
            drw.text(x, 0, w, bh, m.sel.name, 0);
            drw.rect(x, 0, w, bh, m.sel.isfixed, m.sel.isfloating, 0);
        } else {
            drw.setscheme(&scheme[SchemeNorm]);
            drw.text(x, 0, w, bh, null, 0);
        }
    }
    drw.map(m.barwin, 0, 0, m.ww, bh);
}

void drawbars() {
    
    foreach(m; mons.range) {
        drawbar(m);
    }

}

void enternotify(XEvent *e) {
    
    Client *c;
    Monitor *m;
    XCrossingEvent *ev = &e.xcrossing;

    if((ev.mode != NotifyNormal || ev.detail == NotifyInferior) && ev.window != rootWin)
        return;
    c = wintoclient(ev.window);
    m = c ? c.mon : wintomon(ev.window);
    if(m != selmon) {
        unfocus(selmon.sel, true);
        selmon = m;
    } else if(!c || c == selmon.sel) {
        return;
    }
    focus(c);
}

void expose(XEvent *e) {
    

    Monitor *m;
    XExposeEvent *ev = &e.xexpose;

    if(ev.count == 0) {
        m = wintomon(ev.window);
        if(m !is null) {
            drawbar(m);
        }
    }
}

void focus(Client *c) {
    if(!c || !ISVISIBLE(c)) {
        c = selmon.stack.range!"snext".find!(a => ISVISIBLE(a)).front;
    }
    /* was if(selmon.sel) */
    if(selmon.sel && selmon.sel != c)
        unfocus(selmon.sel, false);
    if(c) {
        if(c.mon != selmon)
            selmon = c.mon;
        if(c.isurgent)
            clearurgent(c);
        detachstack(c);
        attachstack(c);
        grabbuttons(c, true);
        XSetWindowBorder(dpy, c.win, scheme[SchemeSel].border.rgb);
        setfocus(c);
    } else {
        XSetInputFocus(dpy, rootWin, RevertToPointerRoot, CurrentTime);
        XDeleteProperty(dpy, rootWin, netatom[NetActiveWindow]);
    }
    selmon.sel = c;
    drawbars();
}

void focusin(XEvent *e) {  /* there are some broken focus acquiring clients */  
    XFocusChangeEvent *ev = &e.xfocus;
    if(selmon.sel && ev.window != selmon.sel.win) {
        setfocus(selmon.sel);
    }
}

void focusmon(const Arg *arg) {
    
    Monitor *m;

    if(mons && !mons.next) {
        return;
    }
    m = dirtomon(arg.i);
    if(m == selmon) {
        return;
    }
    unfocus(selmon.sel, false); /* s/true/false/ fixes input focus issues
					in gedit and anjuta */
    selmon = m;
    focus(null);
}
void focusstack(const Arg *arg) {
    
    Client *c = null, i;

    if(!selmon.sel)
        return;
    if(arg.i > 0) {
        c = selmon.sel.range!"next".find!(a => ISVISIBLE(a)).front;
        if(!c) {
            c = selmon.clients.range!"next".find!(a => ISVISIBLE(a)).front;
        }
    } else {
        for(i = selmon.clients; i != selmon.sel; i = i.next) {
            if(ISVISIBLE(i)) {
                c = i;
            }
        }
        if(!c) {
            for(; i; i = i.next) {
                if(ISVISIBLE(i)) {
                    c = i;
                }
            }
        }
    }
    if(c) {
        focus(c);
        restack(selmon);
    }
}


Atom getatomprop(Client *c, Atom prop) {
    
    int di;
    ulong dl;
    ubyte* p = null;
    Atom da, atom = None;

    if(XGetWindowProperty(dpy, c.win, prop, 0L, atom.sizeof, false, XA_ATOM,
                          &da, &di, &dl, &dl, &p) == XErrorCode.Success && p) {
        atom = *cast(Atom *)(p);
        XFree(p);
    }
    return atom;
}

bool getrootptr(int *x, int *y) {
    
    int di;
    uint dui;
    Window dummy;

    return XQueryPointer(dpy, rootWin, &dummy, &dummy, x, y, &di, &di, &dui) != 0;
}



long getstate(Window w) {
    int format;
    long result = -1;
    ubyte *p = null;
    ulong n, extra;
    Atom realVal;

    if(XGetWindowProperty(dpy, w, wmatom[WMState], 0L, 2L, false, wmatom[WMState],
                          &realVal, &format, &n, &extra, cast(ubyte **)&p) != XErrorCode.Success) {
        return -1;
    }
    if(n != 0) {
        result = *p;
    }
    XFree(p);
    return result;
}

bool gettextprop(Window w, Atom atom, out string text) {
    
    static immutable size_t MAX_TEXT_LENGTH = 256;
    XTextProperty name;
    XGetTextProperty(dpy, w, &name, atom);
    if(!name.nitems)
        return false;
    if(name.encoding == XA_STRING) {
        text = (cast(char*)(name.value)).fromStringz.to!string;
    } else {
        char **list = null;
        int n;
        if(XmbTextPropertyToTextList(dpy, &name, &list, &n) >= XErrorCode.Success &&
                n > 0 &&
                *list) {
            text = (*list).fromStringz.to!string;
            XFreeStringList(list);
        }
    }
    XFree(name.value);
    return true;
}

void grabbuttons(Client *c, bool focused) {
    
    updatenumlockmask();
    uint i, j;
    uint[] modifiers = [ 0, LockMask, numlockmask, numlockmask|LockMask ];
    XUngrabButton(dpy, AnyButton, AnyModifier, c.win);
    if(focused) {
        foreach(ref const but; buttons) {
            if(but.click == ClkClientWin) {
                foreach(ref const mod; modifiers) {
                    XGrabButton(dpy, but.button,
                                but.mask | mod,
                                c.win, false, BUTTONMASK,
                                GrabModeAsync, GrabModeSync,
                                cast(ulong)None, cast(ulong)None);
                }
            }
        }
    } else {
        XGrabButton(dpy, AnyButton, AnyModifier, c.win, false,
                    BUTTONMASK, GrabModeAsync, GrabModeSync, None, None);
    }
}

void grabkeys() {
    
    updatenumlockmask();
    {
        uint i, j;
        uint[] modifiers = [ 0, LockMask, numlockmask, numlockmask|LockMask ];
        KeyCode code;

        XUngrabKey(dpy, AnyKey, AnyModifier, rootWin);
        foreach(ref const key; keys) {
            code = XKeysymToKeycode(dpy, key.keysym);
            if(code) {
                foreach(ref const mod; modifiers) {
                    XGrabKey(dpy, code, key.mod | mod, rootWin,
                             True, GrabModeAsync, GrabModeAsync);
                }
            }
        }
    }
}


void incnmaster(const Arg *arg) {
    
    selmon.nmaster = max(selmon.nmaster + arg.i, 0);
    arrange(selmon);
}

version(XINERAMA) {

    static bool isuniquegeom(XineramaScreenInfo *unique, size_t n, XineramaScreenInfo *info) {
        while(n--) {
            if(unique[n].x_org == info.x_org &&
                    unique[n].y_org == info.y_org &&
                    unique[n].width == info.width &&
                    unique[n].height == info.height) {
                return false;
            }
        }
        return true;
    }
} /* XINERAMA */


void keypress(XEvent *e) {
    
    uint i;
    KeySym keysym;
    XKeyEvent *ev;

    ev = &e.xkey;
    keysym = XKeycodeToKeysym(dpy, cast(KeyCode)ev.keycode, 0);
    foreach(ref const key; keys) {
        if(keysym == key.keysym
                && CLEANMASK(key.mod) == CLEANMASK(ev.state)
                && key.func) {
            key.func( &(key.arg) );
        }
    }
}

void killclient(const Arg *arg) {
    
    if(!selmon.sel)
        return;
    if(!sendevent(selmon.sel, wmatom[WMDelete])) {
        XGrabServer(dpy);
        XSetErrorHandler(&xerrordummy);
        XSetCloseDownMode(dpy, CloseDownMode.DestroyAll);
        XKillClient(dpy, selmon.sel.win);
        XSync(dpy, false);
        XSetErrorHandler(&xerror);
        XUngrabServer(dpy);
    }
}

void manage(Window w, XWindowAttributes *wa) {
    
    Client *c, t = null;
    Window trans = None;
    XWindowChanges wc;

    c = new Client();
    if(c is null) {
        die("fatal: could not malloc() %u bytes\n", Client.sizeof);
    }
    c.win = w;
    updatetitle(c);

    c.mon = null;
    if(XGetTransientForHint(dpy, w, &trans)) {
        t = wintoclient(trans);
        if(t) {
            c.mon = t.mon;
            c.tags = t.tags;
        }
    } 
    if(!c.mon) {
        c.mon = selmon;
        applyrules(c);
    }
    /* geometry */
    c.x = c.oldx = wa.x;
    c.y = c.oldy = wa.y;
    c.w = c.oldw = wa.width;
    c.h = c.oldh = wa.height;
    c.oldbw = wa.border_width;

    if(c.x + WIDTH(c) > c.mon.mx + c.mon.mw)
        c.x = c.mon.mx + c.mon.mw - WIDTH(c);
    if(c.y + HEIGHT(c) > c.mon.my + c.mon.mh)
        c.y = c.mon.my + c.mon.mh - HEIGHT(c);
    c.x = max(c.x, c.mon.mx);
    /* only fix client y-offset, if the client center might cover the bar */
    c.y = max(c.y, ((c.mon.by == c.mon.my) && (c.x + (c.w / 2) >= c.mon.wx)
                    && (c.x + (c.w / 2) < c.mon.wx + c.mon.ww)) ? bh : c.mon.my);
    c.bw = borderpx;

    wc.border_width = c.bw;
    XConfigureWindow(dpy, w, CWBorderWidth, &wc);
    XSetWindowBorder(dpy, w, scheme[SchemeNorm].border.rgb);
    configure(c); /* propagates border_width, if size doesn't change */
    updatewindowtype(c);
    updatesizehints(c);
    updatewmhints(c);
    XSelectInput(dpy, w, EnterWindowMask|FocusChangeMask|PropertyChangeMask|StructureNotifyMask);
    grabbuttons(c, false);
    if(!c.isfloating)
        c.isfloating = c.oldstate = trans != None || c.isfixed;
    if(c.isfloating)
        XRaiseWindow(dpy, c.win);
    attach(c);
    attachstack(c);
    XChangeProperty(dpy, rootWin, netatom[NetClientList], XA_WINDOW, 32, PropModeAppend,
                    cast(ubyte*)(&(c.win)), 1);
    XMoveResizeWindow(dpy, c.win, c.x + 2 * sw, c.y, c.w, c.h); /* some windows require this */
    setclientstate(c, NormalState);
    if (c.mon == selmon)
        unfocus(selmon.sel, false);
    c.mon.sel = c;
    arrange(c.mon);
    XMapWindow(dpy, c.win);
    focus(null);
}

void mappingnotify(XEvent *e) {
    
    XMappingEvent *ev = &e.xmapping;

    XRefreshKeyboardMapping(ev);
    if(ev.request == MappingKeyboard)
        grabkeys();
}

void maprequest(XEvent *e) {
    
    static XWindowAttributes wa;
    XMapRequestEvent *ev = &e.xmaprequest;

    if(!XGetWindowAttributes(dpy, ev.window, &wa))
        return;
    if(wa.override_redirect)
        return;
    if(!wintoclient(ev.window))
        manage(ev.window, &wa);
}

void monocle(Monitor *m) {
    
    uint n = 0;

    n = m.clients.range!"next".map!(a=>ISVISIBLE(a)).sum;
    if(n > 0) { /* override layout symbol */
        m.ltsymbol = format("[%d]", n);
    }
    for(auto c = nexttiled(m.clients); c; c = nexttiled(c.next)) {
        resize(c, m.wx, m.wy, m.ww - 2 * c.bw, m.wh - 2 * c.bw, false);
    }
}

void motionnotify(XEvent *e) {
    
    static Monitor *mon = null;
    Monitor *m;
    XMotionEvent *ev = &e.xmotion;

    if(ev.window != rootWin)
        return;
    if((m = recttomon(ev.x_root, ev.y_root, 1, 1)) != mon && mon) {
        unfocus(selmon.sel, true);
        selmon = m;
        focus(null);
    }
    mon = m;
}

void movemouse(const Arg *arg) {
    
    int x, y, ocx, ocy, nx, ny;
    Client *c;
    Monitor *m;
    XEvent ev;
    Time lasttime = 0;

    c = selmon.sel;
    if(!c) {
        return;
    }
    if(c.isfullscreen) /* no support moving fullscreen windows by mouse */
        return;
    restack(selmon);
    ocx = c.x;
    ocy = c.y;
    if(XGrabPointer(dpy,
                    rootWin,
                    false,
                    MOUSEMASK,
                    GrabModeAsync,
                    GrabModeAsync,
                    None,
                    cursor[CurMove].cursor,
                    CurrentTime) != GrabSuccess) {
        return;
    }
    if(!getrootptr(&x, &y)) {
        return;
    }
    do {
        XMaskEvent(dpy, MOUSEMASK|ExposureMask|SubstructureRedirectMask, &ev);
        switch(ev.type) {
            case ConfigureRequest:
            case Expose:
            case MapRequest:
                handler[ev.type](&ev);
                break;
            case MotionNotify:
                if ((ev.xmotion.time - lasttime) <= (1000 / 60))
                    continue;
                lasttime = ev.xmotion.time;

                nx = ocx + (ev.xmotion.x - x);
                ny = ocy + (ev.xmotion.y - y);
                if(nx >= selmon.wx && nx <= selmon.wx + selmon.ww
                        && ny >= selmon.wy && ny <= selmon.wy + selmon.wh) {
                    if(abs(selmon.wx - nx) < snap)
                        nx = selmon.wx;
                    else if(abs((selmon.wx + selmon.ww) - (nx + WIDTH(c))) < snap)
                        nx = selmon.wx + selmon.ww - WIDTH(c);
                    if(abs(selmon.wy - ny) < snap)
                        ny = selmon.wy;
                    else if(abs((selmon.wy + selmon.wh) - (ny + HEIGHT(c))) < snap)
                        ny = selmon.wy + selmon.wh - HEIGHT(c);
                    if(!c.isfloating && selmon.lt[selmon.sellt].arrange
                            && (abs(nx - c.x) > snap || abs(ny - c.y) > snap))
                        togglefloating(null);
                }
                if(!selmon.lt[selmon.sellt].arrange || c.isfloating)
                    resize(c, nx, ny, c.w, c.h, true);
                break;
            default :
                break;
        }
    } while(ev.type != ButtonRelease);
    XUngrabPointer(dpy, CurrentTime);
    if((m = recttomon(c.x, c.y, c.w, c.h)) != selmon) {
        sendmon(c, m);
        selmon = m;
        focus(null);
    }
}

Client* nexttiled(Client *c) {
    
    return c.range!"next".find!(a => !a.isfloating && ISVISIBLE(a)).front;
}

void pop(Client *c) {
    
    detach(c);
    attach(c);
    focus(c);
    arrange(c.mon);
}

void propertynotify(XEvent *e) {
    
    Client *c;
    Window trans;
    XPropertyEvent *ev = &e.xproperty;
    if((ev.window == rootWin) && (ev.atom == XA_WM_NAME))
        updatestatus();
    else if(ev.state == PropertyDelete)
        return; /* ignore */
    else {
        c = wintoclient(ev.window);
        if(c) {
            switch(ev.atom) {
                default:
                    break;
                case XA_WM_TRANSIENT_FOR:
                    if(!c.isfloating && (XGetTransientForHint(dpy, c.win, &trans))) {
                        c.isfloating = (wintoclient(trans) !is null);
                        if(c.isfloating) {
                            arrange(c.mon);
                        }
                    }
                    break;
                case XA_WM_NORMAL_HINTS:
                    updatesizehints(c);
                    break;
                case XA_WM_HINTS:
                    updatewmhints(c);
                    drawbars();
                    break;
            }
            if(ev.atom == XA_WM_NAME || ev.atom == netatom[NetWMName]) {
                updatetitle(c);
                if(c == c.mon.sel)
                    drawbar(c.mon);
            }
            if(ev.atom == netatom[NetWMWindowType])
                updatewindowtype(c);
        }
    }
}
void quit(const Arg *arg) {
    
    running = false;
}


Monitor* recttomon(int x, int y, int w, int h) {
    
    auto r = selmon;
    int a, area = 0;

    foreach(m; mons.range) {
        a = INTERSECT(x, y, w, h, m);
        if(a > area) {
            area = a;
            r = m;
        }
    }
    return r;
}

void resize(Client *c, int x, int y, int w, int h, bool interact) {
    
    if(applysizehints(c, x, y, w, h, interact))
        resizeclient(c, x, y, w, h);
}

void resizeclient(Client *c, int x, int y, int w, int h) {
    
    XWindowChanges wc;

    c.oldx = c.x;
    c.x = wc.x = x;
    c.oldy = c.y;
    c.y = wc.y = y;
    c.oldw = c.w;
    c.w = wc.width = w;
    c.oldh = c.h;
    c.h = wc.height = h;
    wc.border_width = c.bw;
    XConfigureWindow(dpy, c.win, CWX|CWY|CWWidth|CWHeight|CWBorderWidth, &wc);
    configure(c);
    XSync(dpy, false);
}

void resizemouse(const Arg *arg) {
    
    int ocx, ocy, nw, nh;
    Client *c;
    Monitor *m;
    XEvent ev;
    Time lasttime = 0;

    c = selmon.sel;
    if(!c) {
        return;
    }
    if(c.isfullscreen) /* no support resizing fullscreen windows by mouse */
        return;
    restack(selmon);
    ocx = c.x;
    ocy = c.y;
    if(XGrabPointer(dpy, rootWin, false, MOUSEMASK, GrabModeAsync, GrabModeAsync,
                    None, cursor[CurResize].cursor, CurrentTime) != GrabSuccess)
        return;
    XWarpPointer(dpy, None, c.win, 0, 0, 0, 0, c.w + c.bw - 1, c.h + c.bw - 1);
    do {
        XMaskEvent(dpy, MOUSEMASK|ExposureMask|SubstructureRedirectMask, &ev);
        switch(ev.type) {
            case ConfigureRequest:
            case Expose:
            case MapRequest:
                handler[ev.type](&ev);
                break;
            case MotionNotify:
                if ((ev.xmotion.time - lasttime) <= (1000 / 60))
                    continue;
                lasttime = ev.xmotion.time;

                nw = max(ev.xmotion.x - ocx - 2 * c.bw + 1, 1);
                nh = max(ev.xmotion.y - ocy - 2 * c.bw + 1, 1);
                if(c.mon.wx + nw >= selmon.wx && c.mon.wx + nw <= selmon.wx + selmon.ww
                        && c.mon.wy + nh >= selmon.wy && c.mon.wy + nh <= selmon.wy + selmon.wh) {
                    if(!c.isfloating && selmon.lt[selmon.sellt].arrange
                            && (abs(nw - c.w) > snap || abs(nh - c.h) > snap))
                        togglefloating(null);
                }
                if(!selmon.lt[selmon.sellt].arrange || c.isfloating)
                    resize(c, c.x, c.y, nw, nh, true);
                break;
            default :
                break;
        }
    } while(ev.type != ButtonRelease);
    XWarpPointer(dpy, None, c.win, 0, 0, 0, 0, c.w + c.bw - 1, c.h + c.bw - 1);
    XUngrabPointer(dpy, CurrentTime);
    while(XCheckMaskEvent(dpy, EnterWindowMask, &ev)) {}
    m = recttomon(c.x, c.y, c.w, c.h);
    if(m != selmon) {
        sendmon(c, m);
        selmon = m;
        focus(null);
    }
}

void restack(Monitor *m) {
    
    XEvent ev;
    XWindowChanges wc;

    drawbar(m);
    if(!m.sel)
        return;
    if(m.sel.isfloating || !m.lt[m.sellt].arrange)
        XRaiseWindow(dpy, m.sel.win);
    if(m.lt[m.sellt].arrange) {
        wc.stack_mode = Below;
        wc.sibling = m.barwin;
        auto stacks = m.stack.range!"snext".filter!(a => !a.isfloating && ISVISIBLE(a));
        foreach(c; stacks) {
            XConfigureWindow(dpy, c.win, CWSibling|CWStackMode, &wc);
            wc.sibling = c.win;
        }
    }
    XSync(dpy, false);
    while(XCheckMaskEvent(dpy, EnterWindowMask, &ev)) {}
}

void run() {
    
    extern(C) __gshared XEvent ev;
    /* main event loop */
    XSync(dpy, false);
    while(running && !XNextEvent(dpy, &ev)) {
        if(handler[ev.type]) {
            handler[ev.type](&ev); /* call handler */
        }
    }
}

void scan() {
    
    uint i, num;
    Window d1, d2;
    Window* wins = null;
    XWindowAttributes wa;

    if(XQueryTree(dpy, rootWin, &d1, &d2, &wins, &num)) {
        for(i = 0; i < num; i++) {
            if(!XGetWindowAttributes(dpy, wins[i], &wa)
                    || wa.override_redirect || XGetTransientForHint(dpy, wins[i], &d1))
                continue;
            if(wa.map_state == IsViewable || getstate(wins[i]) == IconicState)
                manage(wins[i], &wa);
        }
        for(i = 0; i < num; i++) { /* now the transients */
            if(!XGetWindowAttributes(dpy, wins[i], &wa))
                continue;
            if(XGetTransientForHint(dpy, wins[i], &d1)
                    && (wa.map_state == IsViewable || getstate(wins[i]) == IconicState))
                manage(wins[i], &wa);
        }
        if(wins)
            XFree(wins);
    }
}

void sendmon(Client *c, Monitor *m) {
    
    if(c.mon == m)
        return;
    unfocus(c, true);
    detach(c);
    detachstack(c);
    c.mon = m;
    c.tags = m.tagset[m.seltags]; /* assign tags of target monitor */
    attach(c);
    attachstack(c);
    focus(null);
    arrange(null);
}

void setclientstate(Client *c, long state) {
    
    long[] data = [ state, None ];

    XChangeProperty(dpy, c.win, wmatom[WMState], wmatom[WMState], 32,
                    PropModeReplace, cast(ubyte *)data, 2);
}

bool
sendevent(Client *c, Atom proto) {
    int n;
    Atom *protocols;
    bool exists = false;
    XEvent ev;

    if(XGetWMProtocols(dpy, c.win, &protocols, &n)) {
        while(!exists && n--)
            exists = protocols[n] == proto;
        XFree(protocols);
    }
    if(exists) {
        ev.type = ClientMessage;
        ev.xclient.window = c.win;
        ev.xclient.message_type = wmatom[WMProtocols];
        ev.xclient.format = 32;
        ev.xclient.data.l[0] = proto;
        ev.xclient.data.l[1] = CurrentTime;
        XSendEvent(dpy, c.win, false, NoEventMask, &ev);
    }
    return exists;
}

void setfocus(Client *c) {
    
    if(!c.neverfocus) {
        XSetInputFocus(dpy, c.win, RevertToPointerRoot, CurrentTime);
        XChangeProperty(dpy, rootWin, netatom[NetActiveWindow],
                        XA_WINDOW, 32, PropModeReplace,
                        cast(ubyte *) &(c.win), 1);
    }
    sendevent(c, wmatom[WMTakeFocus]);
}

void setfullscreen(Client *c, bool fullscreen) {
    
    if(fullscreen) {
        XChangeProperty(dpy, c.win, netatom[NetWMState], XA_ATOM, 32,
                        PropModeReplace, cast(ubyte*)&netatom[NetWMFullscreen], 1);
        c.isfullscreen = true;
        c.oldstate = c.isfloating;
        c.oldbw = c.bw;
        c.bw = 0;
        c.isfloating = true;
        resizeclient(c, c.mon.mx, c.mon.my, c.mon.mw, c.mon.mh);
        XRaiseWindow(dpy, c.win);
    } else {
        XChangeProperty(dpy, c.win, netatom[NetWMState], XA_ATOM, 32,
                        PropModeReplace, cast(ubyte*)0, 0);
        c.isfullscreen = false;
        c.isfloating = c.oldstate;
        c.bw = c.oldbw;
        c.x = c.oldx;
        c.y = c.oldy;
        c.w = c.oldw;
        c.h = c.oldh;
        resizeclient(c, c.x, c.y, c.w, c.h);
        arrange(c.mon);
    }
}

void setlayout(const Arg *arg) {
    if(!arg || !arg.v || arg.v != selmon.lt[selmon.sellt])
        selmon.sellt ^= 1;
    if(arg && arg.v) {
        selmon.lt[selmon.sellt] = cast(Layout *)arg.v;
    }
    selmon.ltsymbol = selmon.lt[selmon.sellt].symbol;
    if(selmon.sel)
        arrange(selmon);
    else
        drawbar(selmon);
}

/* arg > 1.0 will set mfact absolutly */
void setmfact(const Arg *arg) {
    
    float f;

    if(!arg || !selmon.lt[selmon.sellt].arrange)
        return;
    f = arg.f < 1.0 ? arg.f + selmon.mfact : arg.f - 1.0;
    if(f < 0.1 || f > 0.9)
        return;
    selmon.mfact = f;
    arrange(selmon);
}

void setup() {
    
    XSetWindowAttributes wa;

    /* clean up any zombies immediately */
    sigchld(0);

    /* init screen */
    screen = DefaultScreen(dpy);
    rootWin = RootWindow(dpy, screen);
    fnt = new Fnt(dpy, font);
    sw = DisplayWidth(dpy, screen);
    sh = DisplayHeight(dpy, screen);
    bh = fnt.h + 2;
    drw = new Drw(dpy, screen, rootWin, sw, sh);
    drw.setfont(fnt);
    updategeom();
    /* init atoms */
    wmatom[WMProtocols] = XInternAtom(dpy, cast(char*)("WM_PROTOCOLS".toStringz), false);
    wmatom[WMDelete] = XInternAtom(dpy, cast(char*)("WM_DELETE_WINDOW".toStringz), false);
    wmatom[WMState] = XInternAtom(dpy, cast(char*)("WM_STATE".toStringz), false);
    wmatom[WMTakeFocus] = XInternAtom(dpy, cast(char*)("WM_TAKE_FOCUS".toStringz), false);
    netatom[NetActiveWindow] = XInternAtom(dpy, cast(char*)("_NET_ACTIVE_WINDOW".toStringz), false);
    netatom[NetSupported] = XInternAtom(dpy, cast(char*)("_NET_SUPPORTED".toStringz), false);
    netatom[NetWMName] = XInternAtom(dpy, cast(char*)("_NET_WM_NAME".toStringz), false);
    netatom[NetWMState] = XInternAtom(dpy, cast(char*)("_NET_WM_STATE".toStringz), false);
    netatom[NetWMFullscreen] = XInternAtom(dpy, cast(char*)("_NET_WM_STATE_FULLSCREEN".toStringz), false);
    netatom[NetWMWindowType] = XInternAtom(dpy, cast(char*)("_NET_WM_WINDOW_TYPE".toStringz), false);
    netatom[NetWMWindowTypeDialog] = XInternAtom(dpy, cast(char*)("_NET_WM_WINDOW_TYPE_DIALOG".toStringz), false);
    netatom[NetClientList] = XInternAtom(dpy, cast(char*)("_NET_CLIENT_LIST".toStringz), false);
    /* init cursors */
    cursor[CurNormal] = new Cur(drw.dpy, CursorFont.XC_left_ptr);
    cursor[CurResize] = new Cur(drw.dpy, CursorFont.XC_sizing);
    cursor[CurMove] = new Cur(drw.dpy, CursorFont.XC_fleur);
    /* init appearance */
    scheme[SchemeNorm].border = new Clr(drw, normbordercolor);
    scheme[SchemeNorm].bg = new Clr(drw, normbgcolor);
    scheme[SchemeNorm].fg = new Clr(drw, normfgcolor);
    scheme[SchemeSel].border = new Clr(drw, selbordercolor);
    scheme[SchemeSel].bg = new Clr(drw, selbgcolor);
    scheme[SchemeSel].fg = new Clr(drw, selfgcolor);
    /* init bars */
    updatebars();
    updatestatus();
    /* EWMH support per view */
    XChangeProperty(dpy, rootWin, netatom[NetSupported], XA_ATOM, 32,
                    PropModeReplace, cast(ubyte*) netatom, NetLast);
    XDeleteProperty(dpy, rootWin, netatom[NetClientList]);
    /* select for events */
    wa.cursor = cursor[CurNormal].cursor;
    wa.event_mask = SubstructureRedirectMask|SubstructureNotifyMask|ButtonPressMask|PointerMotionMask
                    |EnterWindowMask|LeaveWindowMask|StructureNotifyMask|PropertyChangeMask;
    XChangeWindowAttributes(dpy, rootWin, CWEventMask|CWCursor, &wa);
    XSelectInput(dpy, rootWin, wa.event_mask);
    grabkeys();
    focus(null);
}

void showhide(Client *c) {
    
    if(!c)
        return;
    if(ISVISIBLE(c)) { /* show clients top down */
        XMoveWindow(dpy, c.win, c.x, c.y);
        if((!c.mon.lt[c.mon.sellt].arrange || c.isfloating) && !c.isfullscreen)
            resize(c, c.x, c.y, c.w, c.h, false);
        showhide(c.snext);
    } else { /* hide clients bottom up */
        showhide(c.snext);
        XMoveWindow(dpy, c.win, WIDTH(c) * -2, c.y);
    }
}
extern(C) void sigchldImpl(int unused) nothrow @nogc @system {
    // wait for all child processes to exit.
    while(0 < waitpid(-1, null, WNOHANG)) {}
}
void sigchld(int unused) nothrow {
    if(signal(SIGCHLD, &sigchldImpl) == SIG_ERR) {
        die("Can't install SIGCHLD handler");
    }
    sigchldImpl(unused);
}

void spawn(const Arg *arg) {
    import std.variant;
    Variant v = arg.val;
    const(string[]) args = arg.s;
    if(args[0] == dmenucmd[0]) {
        dmenumon[0] =cast(char)('0' + selmon.num);
    }
    try {
        auto pid = spawnProcess(args);
    } catch {
        die("Failed to spawn '%s'", args);
    }
}

void tag(const Arg *arg) {
    
    if(selmon.sel && arg.ui & TAGMASK) {
        selmon.sel.tags = arg.ui & TAGMASK;
        focus(null);
        arrange(selmon);
    }
}

void tagmon(const Arg *arg) {
    
    if(!selmon.sel || !mons.next)
        return;
    sendmon(selmon.sel, dirtomon(arg.i));
}

void tile(Monitor *m) {
    
    uint i, n, h, mw, my, ty;
    Client *c;

    for(n = 0, c = nexttiled(m.clients); c; c = nexttiled(c.next), n++) {}
    if(n == 0) {
        return;
    }

    if(n > m.nmaster) {
        mw = cast(uint)(m.nmaster ? m.ww * m.mfact : 0);
    } else {
        mw = m.ww;
    }
    for(i = my = ty = 0, c = nexttiled(m.clients); c; c = nexttiled(c.next), i++) {
        if(i < m.nmaster) {
            h = (m.wh - my) / (min(n, m.nmaster) - i);
            resize(c, m.wx, m.wy + my, mw - (2*c.bw), h - (2*c.bw), false);
            my += HEIGHT(c);
        } else {
            h = (m.wh - ty) / (n - i);
            resize(c, m.wx + mw, m.wy + ty, m.ww - mw - (2*c.bw), h - (2*c.bw), false);
            ty += HEIGHT(c);
        }
    }
}

void togglebar(const Arg *arg) {
    
    selmon.showbar = !selmon.showbar;
    updatebarpos(selmon);
    XMoveResizeWindow(dpy, selmon.barwin, selmon.wx, selmon.by, selmon.ww, bh);
    arrange(selmon);
}

void togglefloating(const Arg *arg) {
    
    if(!selmon.sel)
        return;
    if(selmon.sel.isfullscreen) /* no support for fullscreen windows */
        return;
    selmon.sel.isfloating = !selmon.sel.isfloating || selmon.sel.isfixed;
    if(selmon.sel.isfloating)
        resize(selmon.sel, selmon.sel.x, selmon.sel.y,
               selmon.sel.w, selmon.sel.h, false);
    arrange(selmon);
}

void toggletag(const Arg *arg) {
    
    uint newtags;

    if(!selmon.sel)
        return;
    newtags = selmon.sel.tags ^ (arg.ui & TAGMASK);
    if(newtags) {
        selmon.sel.tags = newtags;
        focus(null);
        arrange(selmon);
    }
}

void toggleview(const Arg *arg) {
    
    uint newtagset = selmon.tagset[selmon.seltags] ^ (arg.ui & TAGMASK);

    if(newtagset) {
        selmon.tagset[selmon.seltags] = newtagset;
        focus(null);
        arrange(selmon);
    }
}

void unfocus(Client *c, bool setfocus) {
    
    if(!c)
        return;
    grabbuttons(c, false);
    XSetWindowBorder(dpy, c.win, scheme[SchemeNorm].border.rgb);
    if(setfocus) {
        XSetInputFocus(dpy, rootWin, RevertToPointerRoot, CurrentTime);
        XDeleteProperty(dpy, rootWin, netatom[NetActiveWindow]);
    }
}

void unmanage(Client *c, bool destroyed) {
    
    Monitor *m = c.mon;
    XWindowChanges wc;

    /* The server grab construct avoids race conditions. */
    detach(c);
    detachstack(c);
    if(!destroyed) {
        wc.border_width = c.oldbw;
        XGrabServer(dpy);
        XSetErrorHandler(&xerrordummy);
        XConfigureWindow(dpy, c.win, CWBorderWidth, &wc); /* restore border */
        XUngrabButton(dpy, AnyButton, AnyModifier, c.win);
        setclientstate(c, WithdrawnState);
        XSync(dpy, false);
        XSetErrorHandler(&xerror);
        XUngrabServer(dpy);
    }
    DGC.free(c);
    focus(null);
    updateclientlist();
    arrange(m);
}

void unmapnotify(XEvent *e) {
    
    Client *c;
    XUnmapEvent *ev = &e.xunmap;

    c=  wintoclient(ev.window);
    if(c) {
        if(ev.send_event)
            setclientstate(c, WithdrawnState);
        else
            unmanage(c, false);
    }
}

void updatebars() {
    
    XSetWindowAttributes wa = {
override_redirect :
        True,
background_pixmap :
        ParentRelative,
event_mask :
        ButtonPressMask|ExposureMask
    };
    foreach(m; mons.range) {
        if (m.barwin)
            continue;
        m.barwin = XCreateWindow(dpy, rootWin, m.wx, m.by, m.ww, bh, 0, DefaultDepth(dpy, screen),
                                 CopyFromParent, DefaultVisual(dpy, screen),
                                 CWOverrideRedirect|CWBackPixmap|CWEventMask, &wa);
        XDefineCursor(dpy, m.barwin, cursor[CurNormal].cursor);
        XMapRaised(dpy, m.barwin);
    }
}

void updatebarpos(Monitor *m) {
    
    m.wy = m.my;
    m.wh = m.mh;
    if(m.showbar) {
        m.wh -= bh;
        m.by = m.topbar ? m.wy : m.wy + m.wh;
        m.wy = m.topbar ? m.wy + bh : m.wy;
    } else
        m.by = -bh;
}

void updateclientlist() {
    
    Client *c;
    Monitor *m;

    XDeleteProperty(dpy, rootWin, netatom[NetClientList]);
    for(m = mons; m; m = m.next)
        for(c = m.clients; c; c = c.next)
            XChangeProperty(dpy, rootWin, netatom[NetClientList],
                            XA_WINDOW, 32, PropModeAppend,
                            cast(ubyte *)&(c.win), 1);
}

bool updategeom() {
    
    bool dirty = false;

    Bool isXineramaActive = false;

    version(XINERAMA) {
        isXineramaActive = XineramaIsActive(dpy);
    }
    if(isXineramaActive) {
        version(XINERAMA) {
            import std.range;
            int nn;
            Client *c;
            XineramaScreenInfo *info = XineramaQueryScreens(dpy, &nn);
            auto n = mons.range.walkLength;
            XineramaScreenInfo[] unique = new XineramaScreenInfo[nn];
            if(!unique.length) {
                die("fatal: could not malloc() %u bytes\n", XineramaScreenInfo.sizeof * nn);
            }

            /* only consider unique geometries as separate screens */
            int j=0;
            foreach(i; 0..nn) {
                if(isuniquegeom(&unique[j], j, &info[i])) {
                    unique[j++] = info[i];
                }
            }
            XFree(info);
            nn = j;
            if(n <= nn) {
                foreach(i; 0..(nn-n)) { /* new monitors available */
                    auto m = mons.range.find!"a.next is null".front;
                    if(m) {
                        m.next = createmon();
                    } else {
                        mons = createmon();
                    }
                }
                foreach(i, m; iota(nn).lockstep(mons.range)) {
                    if(i >= n ||
                            (unique[i].x_org != m.mx || unique[i].y_org != m.my ||
                             unique[i].width != m.mw || unique[i].height != m.mh)) {
                        dirty = true;
                        m.num = i;
                        m.mx = m.wx = unique[i].x_org;
                        m.my = m.wy = unique[i].y_org;
                        m.mw = m.ww = unique[i].width;
                        m.mh = m.wh = unique[i].height;
                        updatebarpos(m);
                    }
                }
            } else { /* less monitors available nn < n */
                foreach(i; nn..n) {
                    auto m = mons.range.find!"a.next is null".front;
                    if(m) {
                        while(m.clients) {
                            dirty = true;
                            c = m.clients;
                            m.clients = c.next;
                            detachstack(c);
                            c.mon = mons;
                            attach(c);
                            attachstack(c);
                        }
                        if(m == selmon)
                            selmon = mons;
                        cleanupmon(m);
                    }
                }
            }
            unique = null;
        }
    } else {
        /* default monitor setup */
        if(!mons) {
            mons = createmon();
        }
        if(mons.mw != sw || mons.mh != sh) {
            dirty = true;
            mons.mw = mons.ww = sw;
            mons.mh = mons.wh = sh;
            updatebarpos(mons);
        }
    }
    if(dirty) {
        selmon = mons;
        selmon = wintomon(rootWin);
    }
    return dirty;
}

void updatenumlockmask() {
    
    XModifierKeymap *modmap;

    numlockmask = 0;
    modmap = XGetModifierMapping(dpy);
    foreach_reverse(i; 0..7) {
        if(numlockmask == 0) {
            break;
        }
        //for(i = 7; numlockmask == 0 && i >= 0; --i) {
        foreach_reverse(j; 0..modmap.max_keypermod-1) {
            //for(j = modmap.max_keypermod-1; j >= 0; --j) {
            if(modmap.modifiermap[i * modmap.max_keypermod + j] ==
                    XKeysymToKeycode(dpy, XK_Num_Lock)) {
                numlockmask = (1 << i);
                break;
            }
        }
    }
    XFreeModifiermap(modmap);
}

void updatesizehints(Client *c) {
    
    long msize;
    XSizeHints size;

    if(!XGetWMNormalHints(dpy, c.win, &size, &msize)) {
        /* size is uninitialized, ensure that size.flags aren't used */
        size.flags = PSize;
    }
    if(size.flags & PBaseSize) {
        c.basew = size.base_width;
        c.baseh = size.base_height;
    } else if(size.flags & PMinSize) {
        c.basew = size.min_width;
        c.baseh = size.min_height;
    } else {
        c.basew = c.baseh = 0;
    }

    if(size.flags & PResizeInc) {
        c.incw = size.width_inc;
        c.inch = size.height_inc;
    } else {
        c.incw = c.inch = 0;
    }
    if(size.flags & PMaxSize) {
        c.maxw = size.max_width;
        c.maxh = size.max_height;
    } else {
        c.maxw = c.maxh = 0;
    }
    if(size.flags & PMinSize) {
        c.minw = size.min_width;
        c.minh = size.min_height;
    } else if(size.flags & PBaseSize) {
        c.minw = size.base_width;
        c.minh = size.base_height;
    } else {
        c.minw = c.minh = 0;
    }
    if(size.flags & PAspect) {
        c.mina = cast(float)size.min_aspect.y / size.min_aspect.x;
        c.maxa = cast(float)size.max_aspect.x / size.max_aspect.y;
    } else {
        c.maxa = c.mina = 0.0;
    }
    c.isfixed = (c.maxw && c.minw && c.maxh && c.minh
                 && c.maxw == c.minw && c.maxh == c.minh);
}

void updatetitle(Client *c) {
    
    if(!gettextprop(c.win, netatom[NetWMName], c.name)) {
        gettextprop(c.win, XA_WM_NAME, c.name);
    }
    if(c.name.length == 0) { /* hack to mark broken clients */
        c.name = broken;
    }
}

void updatestatus() {
    
    if(!gettextprop(rootWin, XA_WM_NAME, stext)) {
        stext = "ddwm-"~VERSION;
    }
    drawbar(selmon);
}

void updatewindowtype(Client *c) {
    
    Atom state = getatomprop(c, netatom[NetWMState]);
    Atom wtype = getatomprop(c, netatom[NetWMWindowType]);

    if(state == netatom[NetWMFullscreen])
        setfullscreen(c, true);
    if(wtype == netatom[NetWMWindowTypeDialog])
        c.isfloating = true;
}

void updatewmhints(Client *c) {
    
    XWMHints *wmh;
    wmh = XGetWMHints(dpy, c.win);
    if(wmh) {
        if(c == selmon.sel && wmh.flags & XUrgencyHint) {
            wmh.flags &= ~XUrgencyHint;
            XSetWMHints(dpy, c.win, wmh);
        } else
            c.isurgent = (wmh.flags & XUrgencyHint) ? true : false;
        if(wmh.flags & InputHint)
            c.neverfocus = !wmh.input;
        else
            c.neverfocus = false;
        XFree(wmh);
    }
}

void view(const Arg *arg) {
    
    if((arg.ui & TAGMASK) == selmon.tagset[selmon.seltags]) {
        return;
    }
    selmon.seltags ^= 1; /* toggle sel tagset */
    if(arg.ui & TAGMASK) {
        selmon.tagset[selmon.seltags] = arg.ui & TAGMASK;
    }
    focus(null);
    arrange(selmon);
}

Client* wintoclient(Window w) {
    
    foreach(m; mons.range) {
        auto c = m.clients.range!"next".find!(client => client.win == w).front;
        if(c) {
            return c;
        }
    }
    return null;
}

Monitor* wintomon(Window w) {
    
    int x, y;

    if(w == rootWin && getrootptr(&x, &y)) {
        return recttomon(x, y, 1, 1);
    }
    auto m = mons.range.find!(mon => mon.barwin == w).front;
    if(m) {
        return m;
    }

    auto c = wintoclient(w);
    if(c) {
        return c.mon;
    }
    return selmon;
}

/* There's no way to check accesses to destroyed windows, thus those cases are
 * ignored (especially on UnmapNotify's).  Other types of errors call Xlibs
 * default error handler, which may call exit.  */
extern(C) int xerror(Display *dpy, XErrorEvent *ee) nothrow {
import deimos.X11.Xproto :
    X_SetInputFocus, X_PolyText8, X_PolyFillRectangle, X_PolySegment,
    X_ConfigureWindow, X_GrabButton, X_GrabKey, X_CopyArea;

    if(ee.error_code == XErrorCode.BadWindow
    || (ee.request_code == X_SetInputFocus && ee.error_code == XErrorCode.BadMatch)
    || (ee.request_code == X_PolyText8 && ee.error_code == XErrorCode.BadDrawable)
    || (ee.request_code == X_PolyFillRectangle && ee.error_code == XErrorCode.BadDrawable)
    || (ee.request_code == X_PolySegment && ee.error_code == XErrorCode.BadDrawable)
    || (ee.request_code == X_ConfigureWindow && ee.error_code == XErrorCode.BadMatch)
    || (ee.request_code == X_GrabButton && ee.error_code == XErrorCode.BadAccess)
    || (ee.request_code == X_GrabKey && ee.error_code == XErrorCode.BadAccess)
    || (ee.request_code == X_CopyArea && ee.error_code == XErrorCode.BadDrawable)) {
        lout(">>>> XERROR: %d", ee.error_code);
        return 0;
    }
    try {
        lout("ddwm: fatal error: request code=%d, error code=%d",
        ee.request_code, ee.error_code);
        fprintf(stderr.getFP, format("ddwm: fatal error: request code=%d, error code=%d\n",
        ee.request_code, ee.error_code).toStringz);
    } catch {
        die(__FUNCTION__~"\n\t--> call to 'fprintf(stderr...)' failed!");
    }
    return xerrorxlib(dpy, ee); /* may call exit */
}

extern(C) int xerrordummy(Display *dpy, XErrorEvent *ee) nothrow {
    return 0;
}

/* Startup Error handler to check if another window manager
 * is already running. */
extern(C) int xerrorstart(Display *dpy, XErrorEvent *ee) nothrow {
    die("ddwm: another window manager is already running");
    return -1;
}

void zoom(const Arg *arg) {
    
    Client *c = selmon.sel;

    if(!selmon.lt[selmon.sellt].arrange ||
            (selmon.sel && selmon.sel.isfloating)) {
        return;
    }
    if(c == nexttiled(selmon.clients)) {
        if(c) {
            c = nexttiled(c.next);
        }
        if(!c) {
            return;
        }
    }
    pop(c);
}


int main(string[] args) {
    
    lout("DDWM-"~VERSION~" start");
    if(args.length == 2 && args[1] == "-v") {
        stderr.writeln("ddwm-"~VERSION~"\n"~
                       "The D port of DWM - The Dynamic Window Manager which sucks less.\n\t"~
                       "© 2014 ddwm porters, see LICENSE for details\n\t"~
                       "© 2006-2011 dwm engineers, see LICENSE for details");
        return -1;
    } else if(args.length != 1) {
        stderr.writeln("usage: ddwm [-v]");
        return -1;
    }

    if(!setlocale(LC_CTYPE, "".toStringz) || !XSupportsLocale()) {
        stderr.writeln("warning: no locale support");
        return -1;
    }
    dpy = XOpenDisplay(null);
    if(dpy is null) {
        stderr.writeln("ddwm: cannot open display");
        return -1;
    }
    checkotherwm();
    setup();
    scan();
    run();
    cleanup();
    XCloseDisplay(dpy);
    lout("DDWM-"~VERSION~" end");
    return 0;
}

