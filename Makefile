DEIMOS-X11 = ../ext/libX11

ddwm:
	dmd -O -release -inline -boundscheck=off -I$(DEIMOS-X11) -L-L$(DEIMOS-X11)/lib -L-lDX11-dmd -L-lX11 source/*.d -ofbuild-artefacts/ddwm

all: ddwm

clean-all:
	rm build-artefacts/*

clean:
	rm build-artefacts/ddwm.o

